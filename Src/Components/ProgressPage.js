import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Text, Left, Right, Icon,Body } from 'native-base';
export default class ListItemSelectedExample extends Component {
  render() {
    return (
      <Container>
        <Content>
          <List style={{margin:10,backgroundColor:"grey"}}>
            <ListItem selected>
              <Left>
                <Text style={{color:'white'}}>AWB NO:</Text>
              </Left>
              <Body>
              <Text style={{color:'white'}}>494058398155</Text>
              </Body>
            </ListItem>
            <ListItem selected>
              <Left>
                <Text style={{color:'white'}}>PRODUCT NAME:</Text>
              </Left>
              <Body>
              <Text style={{color:'white'}}>Lorem ipsum</Text>
              </Body>
            </ListItem>
            <ListItem selected>
              <Left>
                <Text style={{color:'white'}}>ORDER DATE:</Text>
              </Left>
              <Body>
              <Text style={{color:'white'}}>21st Feb, 2019</Text>
              </Body>
            </ListItem>
            <ListItem selected>
              <Left>
                <Text style={{color:'white'}}>ORDER STATUS:</Text>
              </Left>
              <Body>
              <Text style={{color:'white',color:'red'}}>On Process</Text>
              </Body>
            </ListItem>
            <ListItem selected>
              <Left>
                <Text style={{color:'white'}}>WEIGHT(KG):</Text>
              </Left>
              <Body>
              <Text style={{color:'white'}}>0.85 KG</Text>
              </Body>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}