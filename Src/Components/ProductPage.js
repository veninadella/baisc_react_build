import React, { Component } from 'react';
import { Alert, View, Text, StyleSheet, TextInput, TouchableOpacity, Image, ImageBackground } from 'react-native';

export default class ProductPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            getTitle:{
                key: ''
            }
        };
    }
    handleChange(name, e) {
        var change = this.state.getTitle;
        var strValue = e;
        change[name] = strValue;
        this.setState({ getTitle: change })

    }
    getProductDetails() {
        // Alert.alert('Product', 'success')
        var formData = new FormData();
        const { navigate } = this.props.navigation;
        // let key = this.state.getTitle.password
        // let key = 494058398155
        let key1 = this.state.getTitle.key;
        // Alert.alert('Key value is', key1)
        formData.append('key', key1);
        if (key1 == undefined) {
            Alert.alert('password', 'Please enter Enter your AWB No.(or) Ref No')
        }
        else {
            // Alert.alert('Key value is', key1)
            fetch('http://gvknew.clientdemos.in/Api/getRecords', {
                method: 'POST',
                body: formData
            }).then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.data) {
                        navigate('Details', { data: responseJson })
                        // Alert.alert('success', JSON.stringify(responseJson))
                    }
                    // else {
                    //     Alert.alert('Plese Enter valid AWB No.(or) Ref No')
                    // }
                })
                .catch((error) => {
                    console.error(error);
                });;

        }
        // else{
        //     Alert.alert('password', 'Please enter Enter your AWB No.(or) Ref No')
        // }
    }
    render() {
        return (
            <ImageBackground style={styles.bg_img} imgStyle={{ opacity: 6.8 }} source={require('../Images/banner-2.jpg')}>
                <View style={styles.container}>
                    <Image source={require('../Images/gvk.png')} style={{ margin: 15 }} />
                    <Text style={styles.txtStyle} onPress={() => Linking.openURL(url)}>FAST</Text>
                    <Text style={styles.txtStyle} onPress={() => Linking.openURL(url)}>SECURED</Text>
                    <Text style={styles.txtStyle} onPress={() => Linking.openURL(url)}>WORLDWIDE</Text>
                    <Text style={{ color: 'white', fontSize: 26, fontWeight: 'bold' }}>TRACK YOUR </Text>
                    <Text style={{ color: '#ee1c25', fontSize: 26, fontWeight: 'bold' }}>PRODUCT</Text>
                    <Text style={{ color: 'white', fontSize: 14, fontWeight: 'bold', margin: 15 }}>TRACK YOUR PRODUCT WITH AWB NO.(OR) RFE NO.</Text>
                    <View style={styles.button}>
                        <TextInput style={styles.txtStyle} placeholder="Enter your AWB No.(or) Ref No." placeholderTextColor='white'
                            value={this.state.getTitle.key} onChangeText={this.handleChange.bind(this, 'key')} />
                    </View>
                    <TouchableOpacity style={styles.button1} onPress={this.getProductDetails.bind(this)}>
                        <Text style={{ textAlign: 'center', fontSize: 18, color: 'white' }}>GET STATUS</Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    bg_img: {
        width: '100%',
        height: '100%',
        // shadowOpacity:0.7
    },
    txtStyle: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 16
    },
    button: {
        borderWidth: 2,
        backgroundColor: 'grey',
        borderColor: 'grey',
        padding: 10,
        margin: 15,
        width: 300,
        height: 60
    },
    button1: {
        borderWidth: 2,
        backgroundColor: '#ee1c25',
        borderColor: '#ee1c25',
        padding: 3,
        width: 300,
        height: 40
    }

})

