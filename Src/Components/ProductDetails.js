import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import StepIndicator from 'react-native-step-indicator';
import { Left, Button, Body, Title, Right, Icon, List, ListItem } from 'native-base';
// import { Icon } from 'react-native-elements';

const labels = ["USA", "SIGAPORE", "INDIA"];
const customStyles = {
  stepIndicatorSize: 10,
  currentStepIndicatorSize: 10,
  separatorStrokeWidth: 1,
  // separatorStrokeFinishedWidth:0.5,
  // separatorStrokeUnfinishedWidth:1,
  currentStepStrokeWidth: 10,
  stepStrokeCurrentColor: 'red',
  stepStrokeWidth: 6,
  stepStrokeFinishedColor: 'black',
  stepStrokeUnFinishedColor: '#aaaaaa',
  separatorFinishedColor: 'grey',
  separatorUnFinishedColor: '#aaaaaa',
  stepIndicatorFinishedColor: '#fe7013',
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: '#fe7013',
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: '#aaaaaa',
  labelColor: '#999999',
  labelSize: 13,
  currentStepLabelColor: '#fe7013',
  borderRadius: 5,
  borderStyle: 'dotted',
}

export default class ProductDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
        currentPosition: 1,
    };
  }
  onPageChange(position) {
    this.setState({ currentPosition: position });
  }

  render() {
    const { navigation } = this.props;
    const data = navigation.getParam('data', 'NO-ID');
    return (
      <View style={styles.container}>
          <View style={{flex:0.08, justifyContent:'flex-start', alignItems:'flex-start'}}>
          <TouchableOpacity onPress={()=>this.props.navigation.navigate('Product')}>
          <Icon style={{ color: "black", margin: 10, marginTop: 10, justifyContent: "flex-start" }} name="arrow-back" />
          {/* <Icon name="keyboard-backspace" color="#5c8fc8" size={30} /> */}
          </TouchableOpacity>
          </View>
          <View style={{flex:0.85}}>
              <Text style={{fontSize: 28, fontWeight: 'bold', color: 'black', opacity: 0.7, textAlign: 'center'}}>PRODUCT TRACKING</Text>
              <Text style={{fontSize: 14, fontWeight: 'bold', color: '#c9c9c9', textAlign: 'center', margin:10}}>Track your product & see the current condition</Text>
              <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'black', opacity: 0.6, textAlign: 'center'}}>TRACK YOUR PRODUCT</Text>
            <Text style={{ fontSize: 12, fontWeight: 'bold', color: '#c9c9c9', opacity: 0.3, textAlign: 'center', margin:10}}>Now you can track your product easily</Text>
            <View style={styles.button}>
            <TextInput style={styles.txtStyle} placeholder="Enter your Product ID." placeholderTextColor='black' />
            </View>
            <TouchableOpacity style={styles.button1}>
            <Text style={{ textAlign: 'center', fontSize: 18, color: 'white' }}>GET STATUS</Text>
            </TouchableOpacity>
            <StepIndicator
            customStyles={customStyles}
            currentPosition={this.state.currentPosition}
            labels={labels}
          />
          </View>
          <View style={{flex:0.85}}>
          <List style={{ margin: 25, backgroundColor: "#222222" }}>
            <ListItem selected>
              <Left>
                <Text style={{ color: 'white' }}>AWB NO:</Text>
              </Left>
              <Body>
                <Text style={{ color: 'white' }}>{data.data[0].AWB_no}</Text>
              </Body>
            </ListItem>
            <ListItem selected>
              <Left>
                <Text style={{ color: 'white' }}>PRODUCT NAME:</Text>
              </Left>
              <Body>
                <Text style={{ color: 'white' }}>{data.data[0].Vendor_name}</Text>
              </Body>
            </ListItem>
            <ListItem selected>
              <Left>
                <Text style={{ color: 'white' }}>ORDER DATE:</Text>
              </Left>
              <Body>
                <Text style={{ color: 'white' }}>{data.data[0].Date}</Text>
              </Body>
            </ListItem>
            <ListItem selected>
              <Left>
                <Text style={{ color: 'white' }}>ORDER STATUS:</Text>
              </Left>
              <Body>
                <Text style={{ color: 'white', color: 'red' }}>{data.data[0].Status}</Text>
              </Body>
            </ListItem>
            <ListItem selected>
              <Left>
                <Text style={{ color: 'white' }}>WEIGHT(KG):</Text>
              </Left>
              <Body>
                <Text style={{ color: 'white' }}>0.85 KG</Text>
              </Body>
            </ListItem>
            <ListItem selected>
              <Left>
                <Text style={{ color: 'white' }}>COUNTRY:</Text>
              </Left>
              <Body>
                <Text style={{ color: 'white' }}>{data.data[0].Country}</Text>
              </Body>
            </ListItem>
          </List>
          </View>
      </View>
    );
  }
}
const styles=StyleSheet.create({
    container:{
        flex:1
    },
    button: {
        borderWidth: 2,
        backgroundColor: 'white',
        borderColor: 'black',
        padding: 10,
        margin: 10,
        width: 300,
        height: 60,
        marginLeft: 50,      
    },
    button1: {
        borderWidth: 2,
        backgroundColor: '#ee1c25',
        borderColor: '#ee1c25',
        padding: 3,
        width: 300,
        height: 60,
        marginLeft: 50,
        margin: 10,
    }
})