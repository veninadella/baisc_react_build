import React, { Component } from 'react';
import { Alert, View, Text, StyleSheet, TextInput, TouchableOpacity, Image, ImageBackground } from 'react-native';


class homeComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {            
            getTitle: {
                key: '',
            },
            modalVisible: false,
        };
        this.inputs = {};
        this.focusNextField = this.focusNextField.bind(this);
    }
    focusNextField(id) {
        this.inputs[id].focus();
    }
    handleChange(name, e) {
        var change = this.state.getTitle;
        var strValue = e;
        change[name] = strValue;
        this.setState({ getTitle: change })

    }
    loginMethod() {
        var formData = new FormData();
        const { navigate } = this.props.navigation;
        // let key = this.state.getTitle.password
        // let key = 494058398155
        let key1 = this.state.getTitle.key;
        // Alert.alert('Key value is', key1)
        formData.append('key', key1);
        if (key1 == undefined) {
            Alert.alert('password', 'Please enter Enter your AWB No.(or) Ref No')
        }
       else{
            // Alert.alert('Key value is', key1)
            fetch('http://gvknew.clientdemos.in/Api/getRecords', {
                method: 'POST',
                body:formData
            }).then((response) => response.json())           
                .then((responseJson) => {
                    if (responseJson.data) {
                        navigate('Details', { data: responseJson })
                    }
                    // else {
                    //     Alert.alert('Plese Enter valid AWB No.(or) Ref No')
                    // }
                })
                .catch((error) => {
                    console.error(error);
                });;

        }
        // else{
        //     Alert.alert('password', 'Please enter Enter your AWB No.(or) Ref No')
        // }
    }
    help() {
        alert("Please enter your details")
    }

    render() {

        return (
            <ImageBackground source={require('../Images/banner-2.png')} style={styles.ImageBackground} imageStyle={{ opacity: 0.1 }}>
                <View style={styles.container}>
                    <View style={styles.main} >
                        <Image source={require('../Images/gvk.png')}
                            style={styles.imageSizer} />

                        <Text style={styles.List}>FAST . SECURE . WORLDWIDE</Text>
                        <Text style={{ fontSize: 23, fontWeight: 'bold', color: 'white', textAlign: 'center' }} >TRACK YOUR</Text>
                        <Text style={{ fontSize: 23, fontWeight: 'bold', color: '#EF3B24', textAlign: 'center' }}>PRODUCT</Text>
                        <Text style={{ margin: 10, fontSize: 13, fontWeight: 'bold', color: 'white', textAlign: 'center' }}>TRACK YOUR PRODUCT WITH AWB NO.(OR) REF NO.</Text>
                        <TextInput transparent
                            placeholder="Enter your AWB No.(or) Ref No"
                            placeholderTextColor="white"
                            // backgroundColor="transparent"
                            style={styles.userTextfields}
                            value={this.state.getTitle.key}
                            onChangeText={this.handleChange.bind(this, 'key')}
                            // ref={input => { this.inputs["one"] = input; }}
                            // onSubmitEditing={() => { this.focusNextField("two"); }}
                        />
                        <TouchableOpacity onPress={this.loginMethod.bind(this)} style={[styles.button, { backgroundColor: '#EF3B24' }]}>
                            <Text style={{ fontSize: 16, color: 'white', textAlign: 'center', marginTop: 5 }}>GET STATUS</Text>
                        </TouchableOpacity>

                    </View>

                </View>
            </ImageBackground >
        );
    }
}

export default homeComponent;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        margin: 1,
    },
    ImageBackground: {
        flex: 1,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#594D63",
        resizeMode: 'contain'

    },

    main: {
        flex: 1,
        justifyContent: 'center',
        margin: 1,
        backgroundColor: "transparent",
    },
    List: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 45,
        color: 'white',
        textAlign: 'center',
        fontSize: 20,
        fontWeight: '500'
    },
    header: {
        flex: 1
    },
    userTextfields: {
        height: 55,
        alignItems: 'center',
        width: 300,
        padding: 10,
        color: 'white',
        margin: 15,
        fontSize: 14,
        fontWeight: "bold",
        marginHorizontal: 16,
        backgroundColor: "#5A4C63",

    },
    button: {
        height: 45,
        borderColor: '#EF3B24',
        width: 300,
        padding: 5,
        marginHorizontal: 16,
        color: 'black',
        textAlign: 'left',
        margin: 5,

    },
    imageSizer: {
        width: 250,
        height: 100,
        alignItems: 'center',
        resizeMode: 'contain',
        justifyContent: "center",
        marginLeft: 40,
    },
    text: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 45,
        borderColor: 'royalblue',
        borderWidth: 1,
        borderRadius: 5,
        padding: 10,
        marginHorizontal: 15,
        color: 'black',
        textAlign: 'center',
        margin: 5,
    },
});

