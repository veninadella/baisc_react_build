import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import StepIndicator from 'react-native-step-indicator';
import { Header, Left, Button, Body,  Icon, Title, Right, Drawer, List, ListItem } from 'native-base';


class details extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPosition: 1,
         };
  }
  onPageChange(position) {
    this.setState({ currentPosition: position });
  }

  render() {
    const { navigation } = this.props;
    const data = navigation.getParam('data', 'NO-ID');
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Button transparent
           onPress={() => 
           }>
          <Icon style={{ color: "black", margin: 10, marginTop: 10, justifyContent: "flex-start" }} name="arrow-back" />
          </Button>
        </View>
        <View style={styles.main}>
          <Text style={{ fontSize: 28, fontWeight: 'bold', color: 'black', opacity: 0.7, textAlign: 'center' }}>PRODUCT TRACKING</Text>
          <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#c9c9c9', textAlign: 'center', marginBottom: 35 }}>Track your product & see the current condition</Text>
          <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'black', opacity: 0.6, textAlign: 'center' }}>TRACK YOUR PRODUCT</Text>
          <Text style={{ fontSize: 12, fontWeight: 'bold', color: 'black', opacity: 0.3, textAlign: 'center' }}>Now you can track your product easily</Text>
          <TextInput transparent
            placeholder="Enter your product ID"
            placeholderTextColor="darkgray"
            style={styles.userTextfields}
          />
          <TouchableOpacity onPress={() => { }} style={[styles.button, { backgroundColor: '#EF3B24' }]}>
            <Text style={{ fontSize: 18, color: 'white', textAlign: 'center', marginTop: 10 }}>Get STATUS</Text>
          </TouchableOpacity>
          <StepIndicator
            customStyles={customStyles}
            currentPosition={this.state.currentPosition}
            labels={labels}
          />
        </View>
        <View style={styles.main}>
          <List style={{ margin: 25, backgroundColor: "#222222" }}>
            <ListItem selected>
              <Left>
                <Text style={{ color: 'white' }}>AWB NO:</Text>
              </Left>
              <Body>
                <Text style={{ color: 'white' }}>{data.data[0].AWB_no}</Text>
              </Body>
            </ListItem>
            <ListItem selected>
              <Left>
                <Text style={{ color: 'white' }}>PRODUCT NAME:</Text>
              </Left>
              <Body>
                <Text style={{ color: 'white' }}>{data.data[0].Vendor_name}</Text>
              </Body>
            </ListItem>
            <ListItem selected>
              <Left>
                <Text style={{ color: 'white' }}>ORDER DATE:</Text>
              </Left>
              <Body>
                <Text style={{ color: 'white' }}>{data.data[0].Date}</Text>
              </Body>
            </ListItem>
            <ListItem selected>
              <Left>
                <Text style={{ color: 'white' }}>ORDER STATUS:</Text>
              </Left>
              <Body>
                <Text style={{ color: 'white', color: 'red' }}>{data.data[0].Status}</Text>
              </Body>
            </ListItem>
            <ListItem selected>
              <Left>
                <Text style={{ color: 'white' }}>WEIGHT(KG):</Text>
              </Left>
              <Body>
                <Text style={{ color: 'white' }}>0.85 KG</Text>
              </Body>
            </ListItem>
            <ListItem selected>
              <Left>
                <Text style={{ color: 'white' }}>COUNTRY:</Text>
              </Left>
              <Body>
                <Text style={{ color: 'white' }}>{data.data[0].Country}</Text>
              </Body>
            </ListItem>
          </List>
        </View>
      </View>
    );
  }
}

export default details;
const labels = ["USA", "SIGAPORE", "INDIA"];
const customStyles = {
  stepIndicatorSize: 10,
  currentStepIndicatorSize: 10,
  separatorStrokeWidth: 1,
  // separatorStrokeFinishedWidth:0.5,
  // separatorStrokeUnfinishedWidth:1,
  currentStepStrokeWidth: 10,
  stepStrokeCurrentColor: 'red',
  stepStrokeWidth: 6,
  stepStrokeFinishedColor: 'black',
  stepStrokeUnFinishedColor: '#aaaaaa',
  separatorFinishedColor: 'grey',
  separatorUnFinishedColor: '#aaaaaa',
  stepIndicatorFinishedColor: '#fe7013',
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: '#fe7013',
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: '#aaaaaa',
  labelColor: '#999999',
  labelSize: 13,
  currentStepLabelColor: '#fe7013',
  borderRadius: 5,
  borderStyle: 'dotted',
}

const styles = StyleSheet.create({
  header: {
    flex: 0.09
  },
  head: { height: 40, backgroundColor: '#f1f8ff' },
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  main: {
    flex: 0.85
  },
  List: {
    flex: 0.1
  },
  userTextfields: {
    height: 45,
    opacity: 0.6,
    width: 300,
    borderWidth: 0.7,
    borderColor: 'black',
    padding: 10,
    color: 'black',
    margin: 10,
    fontSize: 20,
    backgroundColor: 'white',
    marginLeft: 50,
    borderRadius: 3
  },
  button: {
    height: 55,
    width: 300,
    borderColor: 'dodgerblue',
    padding: 5,
    color: 'black',
    margin: 10,
    marginLeft: 50,
    borderRadius: 3,
    marginBottom: 30
  },
});
