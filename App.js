import React, {Component} from 'react';
import { createStackNavigator, createAppContainer} from "react-navigation";
import ProductPage from './Src/Components/ProductPage';
import ProductDetails from './Src/Components/ProductDetails';

const AppNavigator = createStackNavigator(
    {
        Product:{screen:ProductPage},
        Details:{screen:ProductDetails}
    },{
        initialRouteName:'Product',
        headerMode: 'none'
})
const App = createAppContainer(AppNavigator);
export default App;

// import React, { Component } from 'react';
// import { View, Text } from 'react-native';

// class App extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//     };
//   }

//   render() {
//     return (
//       <View style={{backgroundColor:'green'}}>
//         <Text> ProductPage </Text>
//       </View>
//     );
//   }
// }

// export default App;

// /**
//  * Sample React Native App
//  * https://github.com/facebook/react-native
//  *
//  * @format
//  * @flow
//  */

// import React, {Component} from 'react';
// import {Platform, StyleSheet, Text, View} from 'react-native';
//
// const instructions = Platform.select({
//   ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
//   android:
//     'Double tap R on your keyboard to reload,\n' +
//     'Shake or press menu button for dev menu',
// });
//
// type Props = {};
// export default class App extends Component<Props> {
//   render() {
//     return (
//       <View style={styles.container}>
//         <Text style={styles.welcome}>Welcome to React Native!</Text>
//         <Text style={styles.instructions}>To get started, edit App.js</Text>
//         <Text style={styles.instructions}>{instructions}</Text>
//       </View>
//     );
//   }
// }
//
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#F5FCFF',
//   },
//   welcome: {
//     fontSize: 20,
//     textAlign: 'center',
//     margin: 10,
//   },
//   instructions: {
//     textAlign: 'center',
//     color: '#333333',
//     marginBottom: 5,
//   },
// });
